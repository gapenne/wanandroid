package com.gcy.wanandroid.util;

import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CircleCrop;
import com.bumptech.glide.request.RequestOptions;
import com.gcy.wanandroid.R;
import com.gcy.wanandroid.config.App;

import jp.wasabeef.glide.transformations.BlurTransformation;

/**
 * @author: gcy
 * @description: 图片加载工具类
 * @version: 1.0
 * @CreateTime: 2023-05-29  20:01
 */
public class GlideUtil {

    public static void loadImageWithDefault(ImageView imageView, String url) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.placeholder(R.mipmap.ic_logo)
                .error(R.mipmap.ic_logo).fallback(R.mipmap.ic_logo)
                .transform(new CircleCrop());
        Glide.with(App.getContext())
                .load(url)
                .apply(requestOptions)
                .into(imageView);
    }

    public static void loadImageWithGoss(ImageView imageView, String url) {
        Glide.with(App.getContext())
                .load(url)
                .apply(RequestOptions.bitmapTransform(new BlurTransformation(25, 3)))
                .into(imageView);
    }
}
