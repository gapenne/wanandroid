package com.gcy.wanandroid.util;

import java.util.List;

/**
 * @author: gcy
 * @description: 工具类
 * @version: 1.0
 * @CreateTime: 2023-05-20  12:47
 */
public final class CommonUtils {

    private CommonUtils() {

    }

    public static boolean isStringEmpty(String string) {
        return string == null || "".equals(string);
    }

    public static boolean isListEmpty(List<?> list) {
        return list == null || list.size() == 0;
    }
}
