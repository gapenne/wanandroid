package com.gcy.wanandroid.base.adapter;

import android.util.SparseArray;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.List;

/**
 * @author: gcy
 * @description: PagerAdapter的基类
 * @version: 1.0
 * @CreateTime: 2023-05-18  14:50
 */
public abstract class BasePagerAdapter<T> extends FragmentPagerAdapter {

    protected SparseArray<Fragment> mFragment = new SparseArray<>();

    protected List<T> mDataList;

    public BasePagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    public int getCount() {
        return mDataList != null ? mDataList.size() : 0;
    }

    /**
     * @author gcy
     * @description 设置数据列表
     * @date 2023/5/18 14:55
     */
    public void setDataList(List<T> dataList) {
        mDataList = dataList;
        notifyDataSetChanged();
    }

    /**
     * @author gcy
     * @description 释放缓存的Fragment
     * @date 2023/5/18 14:55
     */
    public void release() {
        mFragment.clear();
    }
}
