package com.gcy.wanandroid.base;

import android.app.Activity;
import android.os.Bundle;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gcy.wanandroid.R;
import com.gcy.wanandroid.base.viewmodel.BaseViewModel;
import com.gcy.wanandroid.config.LoadState;
import com.gcy.wanandroid.databinding.FragmentBaseBinding;
import com.gcy.wanandroid.databinding.ViewLoadErrorBinding;
import com.gcy.wanandroid.databinding.ViewLoadingBinding;
import com.gcy.wanandroid.databinding.ViewNoDataBinding;
import com.gcy.wanandroid.databinding.ViewNoNetworkBinding;
import com.gcy.wanandroid.manager.MyActivityManager;
import com.gcy.wanandroid.ui.activity.main.MainActivity;

public abstract class BaseFragment<DB extends ViewDataBinding, VM extends BaseViewModel> extends Fragment {

    public DB mDataBinding;
    public VM mViewModel;

    private FragmentBaseBinding mFragmentBaseBinding;
    private ViewLoadingBinding mViewLoadingBinding;
    private ViewLoadErrorBinding mViewLoadErrorBinding;
    private ViewNoNetworkBinding mViewNoNetworkBinding;
    private ViewNoDataBinding mViewNoDataBinding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            handleArguments(args);
        }
        initViewModel();
        //ViewModel订阅生命周期
        if (mViewModel != null) {
            getLifecycle().addObserver(mViewModel);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         mFragmentBaseBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_base, container, false);
         mDataBinding = DataBindingUtil.inflate(inflater, getLayoutResId(), mFragmentBaseBinding.flContentContainer, true);
         bindViewModel();
         mDataBinding.setLifecycleOwner(this);
         initLoadState();
         initCollectState();
         init();
         return mFragmentBaseBinding.getRoot();
    }

    private void initCollectState() {
        if (mViewModel == null) {
            return;
        }
        mViewModel.getCollectStatus().observe(this, new Observer<Object>() {
            @Override
            public void onChanged(Object collect) {
                if (collect == null) {
                    //跳转到登录页面
                }
            }
        });
    }

    private void initLoadState() {
        if (mViewModel != null && isSupportLoad()) {
            mViewModel.loadState.observe(getViewLifecycleOwner(), new Observer<LoadState>() {
                @Override
                public void onChanged(LoadState loadState) {
                    switchLoadView(loadState);
                }
            });
            Activity activity = MyActivityManager.getInstance().getCurrentActivity();
//            if (activity instanceof MainActivity) {
//
//            }
        }
    }

    private void switchLoadView(LoadState loadState) {
        removeLoadView();

        switch (loadState) {
            case LOADING:
                if (mViewLoadingBinding == null) {
                    mViewLoadingBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.view_loading,
                            mFragmentBaseBinding.flContentContainer, false);
                }
                mFragmentBaseBinding.flContentContainer.addView(mViewLoadingBinding.getRoot());
                break;

            case NO_NETWORK:
                if (mViewNoNetworkBinding == null) {
                    mViewNoNetworkBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.view_no_network,
                            mFragmentBaseBinding.flContentContainer, false);
                    mViewNoNetworkBinding.setViewModel(mViewModel);
                }
                mFragmentBaseBinding.flContentContainer.addView(mViewNoNetworkBinding.getRoot());
                break;

            case NO_DATA:
                if (mViewNoDataBinding == null) {
                    mViewNoDataBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.view_no_data,
                            mFragmentBaseBinding.flContentContainer, false);
                }
                mFragmentBaseBinding.flContentContainer.addView(mViewNoDataBinding.getRoot());
                break;

            case ERROR:
                if (mViewLoadErrorBinding == null) {
                    mViewLoadErrorBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.view_load_error,
                            mFragmentBaseBinding.flContentContainer, false);
                    mViewLoadErrorBinding.setViewModel(mViewModel);
                }
                mFragmentBaseBinding.flContentContainer.addView(mViewLoadErrorBinding.getRoot());
                break;

            default:
                break;
        }
    }

    private void removeLoadView() {
        int childCount = mFragmentBaseBinding.flContentContainer.getChildCount();
        if (childCount > 1) {
            mFragmentBaseBinding.flContentContainer.removeViews(1, childCount - 1);
        }
    }

    protected abstract void init();
    /**
     * @author gcy
     * @description 是否支持页面加载，默认不支持
     * @date 2023/5/18 13:03
     */
    protected boolean isSupportLoad() {
        return false;
    }

    /**
     * @author gcy
     * @description 绑定ViewModel
     * @date 2023/5/18 13:01
     */
    protected abstract void bindViewModel();

    /**
     * @author gcy
     * @description 获取当前页面的布局id
     * @date 2023/5/18 13:01
     */
    protected abstract int getLayoutResId();

    /**
     * @author gcy
     * @description 初始化ViewModel
     * @date 2023/5/18 12:51
     */
    protected abstract void initViewModel();

    /**
     * @author gcy
     * @description 处理参数，args参数容器
     * @date 2023/5/18 12:49
     */
    protected void handleArguments(Bundle args) {
    }


}