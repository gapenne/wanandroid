package com.gcy.wanandroid.ui.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

/**
 * 1. @ClassName CommonAdapter
 * 2. @Description 通用的Adapter
 * 3. @Author gcy
 * 4. @Date 2023/6/28 23:40
 */
public class CommonAdapter<T> extends RecyclerView.Adapter<CommonViewHolder> {

    private List<T> mList;
    /**
     * 布局id
     */
    private int defaultLayout;
    /**
     *
     */
    private int brId;

    public CommonAdapter(int defaultLayout, int brId) {
        this.brId = brId;
        this.defaultLayout = defaultLayout;
    }

    public CommonAdapter(List<T> list, int defaultLayout, int brId) {
        this.mList = list;
        this.defaultLayout = defaultLayout;
        this.brId = brId;
    }

    //添加监听回调
    public void addListener(View root, T itemData, int position) {

    }

    //改变数据
    public void onItemDatasChanged(List<T> newItemDatas) {
        this.mList = newItemDatas;
        notifyDataSetChanged();
    }

    //改变部分数据
    public void onItemRangeChanged(List<T> newItemDatas, int positionStart, int itemCount) {
        this.mList = newItemDatas;
        notifyItemRangeChanged(positionStart, itemCount);
    }

    //插入数据
    protected void onItemRangeInserted(List<T> newItemDatas, int positionStart, int itemCount) {
        this.mList = newItemDatas;
        notifyItemRangeInserted(positionStart, itemCount);
    }

    //移除某个数据
    protected void onItemRangeRemoved(List<T> newItemDatas, int positionStart, int itemCount) {
        this.mList = newItemDatas;
        notifyItemRangeRemoved(positionStart, itemCount);
    }

    public int getItemLayout(T itemData) {
        return defaultLayout;
    }

    public int getItemViewType(int position) {
        return getItemLayout(mList.get(position));
    }

    @NonNull
    @Override
    public CommonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewDataBinding dataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),viewType, parent, false);
        return new CommonViewHolder(dataBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull CommonViewHolder holder, int position) {
        //绑定数据
        holder.binding.setVariable(brId, mList.get(position));
        addListener(holder.binding.getRoot(), mList.get(position), position);
        //防止数据闪烁
        holder.binding.executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return mList != null ? mList.size() : 0;
    }
}
