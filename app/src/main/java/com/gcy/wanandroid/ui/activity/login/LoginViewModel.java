package com.gcy.wanandroid.ui.activity.login;

import android.widget.Toast;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.gcy.wanandroid.base.viewmodel.BaseViewModel;
import com.gcy.wanandroid.bean.responsebean.LoginBean;
import com.gcy.wanandroid.config.App;
import com.gcy.wanandroid.config.Constants;
import com.gcy.wanandroid.http.data.HttpDisposable;
import com.gcy.wanandroid.http.request.HttpFactory;
import com.gcy.wanandroid.http.request.HttpRequest;
import com.gcy.wanandroid.util.CommonUtils;
import com.orhanobut.hawk.Hawk;

/**
 * @author: gcy
 * @description: 登录的ViewModel
 * @version: 1.0
 * @CreateTime: 2023-05-29  20:31
 */
public class LoginViewModel extends BaseViewModel {

    private MutableLiveData<LoginBean> userBean;
    private MutableLiveData<Object> registerStatus;
    public MutableLiveData<String> userName;
    public MutableLiveData<String> userPwd;
    public MutableLiveData<String> userRePwd;

    public LoginViewModel() {
        userBean = new MutableLiveData<>();
        registerStatus = new MutableLiveData<>();
        userName = new MutableLiveData<>();
        userPwd = new MutableLiveData<>();
        userRePwd = new MutableLiveData<>();
    }

    public void clearUserPwd() {
        userPwd.postValue("");
        userRePwd.postValue("");
    }

    public LiveData<LoginBean> getUserBean() {
        return  userBean;
    }

    public  LiveData<Object> getRegisterStatus() {
        return registerStatus;
    }

    /**
    * @Param
    * @return
    * 登录
    */

    public void login() {
        if (CommonUtils.isStringEmpty(userName.getValue()) || CommonUtils.isStringEmpty(userPwd.getValue())) {
            //用户名或密码为空
            return;
        }
        HttpRequest.getInstance()
                .Login(userName.getValue(), userPwd.getValue())
                .compose(HttpFactory.schedulers())
                .subscribe(new HttpDisposable<LoginBean>() {
                    @Override
                    public void success(LoginBean loginBean) {
                        //登录成功，保存用户信息
                        userBean.postValue(loginBean);
                        loginBean.setPassword(userPwd.getValue());
                        Hawk.put(Constants.HawkCode.LOGIN_DATA, loginBean);
                    }

                    @Override
                    public void onError(Throwable e) {
                        Toast.makeText(App.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    /**
    * @Param
    * @return
     * 点击注册
    */

    public void register() {
        HttpRequest.getInstance()
                .register(userName.getValue(), userPwd.getValue(), userRePwd.getValue())
                .compose(HttpFactory.schedulers())
                .subscribe(new HttpDisposable<Object>() {
                    @Override
                    public void success(Object bean) {
                        registerStatus.postValue(bean);
                    }

                    @Override
                    public void onError(Throwable e) {
                        clearUserPwd();
                        Toast.makeText(App.getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
