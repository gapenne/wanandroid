package com.gcy.wanandroid.ui.home;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.gcy.wanandroid.R;
import com.gcy.wanandroid.base.BaseFragment;
import com.gcy.wanandroid.bean.responsebean.ArticleBean;
import com.gcy.wanandroid.bean.responsebean.home.HomeData;
import com.gcy.wanandroid.databinding.FragmentListBinding;
import com.gcy.wanandroid.ui.activity.web.DetailsActivity;
import com.gcy.wanandroid.ui.adapter.HomeAdapter;
import com.scwang.smart.refresh.layout.api.RefreshLayout;
import com.scwang.smart.refresh.layout.listener.OnLoadMoreListener;
import com.scwang.smart.refresh.layout.listener.OnRefreshListener;
import com.zhouwei.mzbanner.MZBannerView;

import java.util.List;

/**
 * @author gcy
 * @description 主页
 * @date 2023/6/27 11:37
 */
public class HomeFragment extends BaseFragment<FragmentListBinding, HomeViewModel> {

    HomeAdapter commonAdapter;
    @Override
    protected void init() {
        mViewModel.loadHomeData();
        initRecycle();
        initRefreshLayout();
    }

    private void initRefreshLayout() {
        mDataBinding.refreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                mViewModel.refreshData(true);
            }
        });
        mDataBinding.refreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                mViewModel.refreshData(false);
            }
        });
    }

    /**
     * 初始化RecycleView
     */
    private void initRecycle() {
        commonAdapter = new HomeAdapter() {
            @Override
            public void addListener(View root, HomeData itemData, int position) {
                super.addListener(root, itemData, position);
                if (itemData.getBannerData() != null) {
                    MZBannerView banner = root.findViewById(R.id.banner);
                    banner.setBannerPageClickListener(new MZBannerView.BannerPageClickListener() {
                        @Override
                        public void onPageClick(View view, int position) {
                            String url = itemData.getBannerData().getBannerData().get(position).getUrl();
                            DetailsActivity.start(getActivity(), url);
                        }
                    });
                } else if (itemData.getTopArticleList() != null) {

                }else {
                    root.findViewById(R.id.card_view).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            DetailsActivity.start(getActivity(), itemData.getArticleList().getLink());
                        }
                    });
                    root.findViewById(R.id.iv_collect).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            itemData.getArticleList().setCollect(!itemData.getArticleList().isCollect());
                            notifyDataSetChanged();
                            mViewModel.changeArticleCollect(itemData.getArticleList().isCollect(), itemData.getArticleList().getId());
                        }
                    });
                }
            }

            @Override
            public void addTopCollectListener(ArticleBean itemData) {
                super.addTopCollectListener(itemData);
                DetailsActivity.start(getActivity(), itemData.getLink());
            }

            @Override
            public void addTopClickListener(ArticleBean itemData) {
                super.addTopClickListener(itemData);
                mViewModel.changeArticleCollect(itemData.isCollect(), itemData.getId());
            }
        };
        mDataBinding.rvRecycle.setLayoutManager(new LinearLayoutManager(getContext()));
        mDataBinding.rvRecycle.setAdapter(commonAdapter);
    }

    @Override
    protected void bindViewModel() {

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initViewModel() {
        mViewModel = new ViewModelProvider(this).get(HomeViewModel.class);
        initDataChange();
    }

    private void initDataChange() {
        mViewModel.getHomeList().observe(this, new Observer<List<HomeData>>() {
            @Override
            public void onChanged(List<HomeData> homeData) {
                commonAdapter.onItemDatasChanged(homeData);
                mDataBinding.refreshLayout.finishRefresh();
                mDataBinding.refreshLayout.finishLoadMore();
            }
        });
    }

    @Override
    public void scrollToTop() {
        mDataBinding.rvRecycle.smoothScrollToPosition(0);
    }

    @Override
    protected boolean isSupportLoad() {
        return true;
    }
}