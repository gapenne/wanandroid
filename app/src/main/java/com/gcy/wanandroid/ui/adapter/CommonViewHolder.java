package com.gcy.wanandroid.ui.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

/**
 * 1. @ClassName CommonViewHolder
 * 2. @Description TODO
 * 3. @Author gcy
 * 4. @Date 2023/6/28 0:19
 */
public class CommonViewHolder extends RecyclerView.ViewHolder {

    ViewDataBinding binding;

    public CommonViewHolder(@NonNull ViewDataBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }
}
