package com.gcy.wanandroid.ui.activity.web;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.LinearLayout;

import com.gcy.wanandroid.R;
import com.gcy.wanandroid.base.BaseActivity;
import com.gcy.wanandroid.config.Constants;
import com.gcy.wanandroid.databinding.ActivityDetailsBinding;
import com.gcy.wanandroid.ui.activity.main.MainActivity;
import com.just.agentweb.AgentWeb;
import com.just.agentweb.WebChromeClient;

public class DetailsActivity extends BaseActivity<ActivityDetailsBinding, DetailsViewModel> {

    private boolean mFromSplash;
    private String mUrl;
    private AgentWeb mAgentWeb;

    public static void start(Context context, String url, boolean fromSplash) {
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(Constants.ParamCode.PARAM1, fromSplash);
        intent.putExtra(Constants.ParamCode.KEY_URL, url);
        context.startActivity(intent);
    }

    public static void start(Context context, String url) {
        Intent intent = new Intent(context, DetailsActivity.class);
        intent.putExtra(Constants.ParamCode.KEY_URL, url);
        context.startActivity(intent);
    }

    @Override
    protected void handleIntent(Intent intent) {
        mUrl = intent.getStringExtra(Constants.ParamCode.KEY_URL);
        mFromSplash = intent.getBooleanExtra(Constants.ParamCode.PARAM1, false);
    }

    @Override
    protected void init() {
        initToolbar();
        initWebView();
    }

    private void initWebView() {
        mAgentWeb = AgentWeb.with(this)
                .setAgentWebParent(mDataBinding.llRoot, new LinearLayout.LayoutParams(
                        ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT
                )).useDefaultIndicator()
                .setWebChromeClient(mWebChromeClient)
                .createAgentWeb()
                .ready()
                .go(mUrl);
    }

    private WebChromeClient mWebChromeClient = new WebChromeClient() {
        @Override
        public void onReceivedTitle(WebView view, String title) {
            mDataBinding.tbToolbar.setTitle(title);
        }
    };

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initToolbar() {
        setSupportActionBar(mDataBinding.tbToolbar);
    }

    @Override
    protected void bindViewModel() {
        mDataBinding.setViewModel(mViewModel);
    }

    @Override
    protected void initViewModel() {
        mViewModel = new DetailsViewModel();
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_details;
    }

    @Override
    protected void onResume() {
        mAgentWeb.getWebLifeCycle().onResume();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        mAgentWeb.getWebLifeCycle().onDestroy();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        mAgentWeb.getWebLifeCycle().onPause();
        super.onPause();
    }

    public void onBackPressed() {
        if (!mAgentWeb.back()) {
            if (mFromSplash) {
                MainActivity.start(DetailsActivity.this, false);
                finish();
            } else {
                super.onBackPressed();
            }
        }
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (mAgentWeb.handleKeyEvent(keyCode, event)) {
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}