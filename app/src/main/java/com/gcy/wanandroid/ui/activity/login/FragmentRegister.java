package com.gcy.wanandroid.ui.activity.login;


import androidx.lifecycle.ViewModelProvider;

import com.gcy.wanandroid.R;
import com.gcy.wanandroid.base.BaseFragment;
import com.gcy.wanandroid.databinding.RegisterFragmentBinding;

/**
 * @author gcy
 * @description 注册页面
 * @date 2023/6/27 11:32
 */
public class FragmentRegister extends BaseFragment<RegisterFragmentBinding, LoginViewModel> {

    @Override
    protected void init() {

    }

    @Override
    protected void bindViewModel() {
        mDataBinding.setViewModel(mViewModel);
        mDataBinding.setActivity((LoginActivity) getActivity());
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.register_fragment;
    }

    @Override
    protected void initViewModel() {
        mViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
    }
}