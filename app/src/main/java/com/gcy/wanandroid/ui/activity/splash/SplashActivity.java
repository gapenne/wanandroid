package com.gcy.wanandroid.ui.activity.splash;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.gcy.wanandroid.R;
import com.gcy.wanandroid.base.BaseActivity;
import com.gcy.wanandroid.config.App;
import com.gcy.wanandroid.config.Constants;
import com.gcy.wanandroid.databinding.ActivitySplashBinding;
import com.gcy.wanandroid.ui.activity.main.MainActivity;
import com.gcy.wanandroid.ui.activity.web.DetailsActivity;
import com.gcy.wanandroid.util.CommonUtils;
import com.orhanobut.hawk.Hawk;

public class SplashActivity extends BaseActivity<ActivitySplashBinding, SplashViewModel> {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        App.firstOpen = true;
        super.onCreate(savedInstanceState);

    }

    protected boolean isNoActionBar() {
        //沉浸式主题
        return true;
    }

    @Override
    protected void init() {
        //加载闪屏页
        mViewModel.getImageData().observe(this, imageBean -> {
            if (imageBean != null) {
                String url = imageBean.getImages().get(0).getBaseUrl() + imageBean.getImages().get(0).getUrl();
                Glide.with(SplashActivity.this)
                        .load(url)
                        .into(mDataBinding.ivSplash);
                Hawk.put(Constants.HawkCode.SPLASH_IMAGE_URL, url);
            } else {
                //从网络获取照片失败，加载本地默认图片
                Glide.with(SplashActivity.this)
                        .load(R.mipmap.splash_bg)
                        .into(mDataBinding.ivSplash);
            }
        });

        //跳转到指定页面
        mViewModel.getActivitySkip().observe(this, activitySkip -> {
            if ("DetailsActivity".equals(activitySkip.getmActivity())) {
                if (!CommonUtils.isStringEmpty(activitySkip.getParam1())) {
                    DetailsActivity.start(SplashActivity.this, activitySkip.getParam1(), true);
                    finish();
                } else {
                    MainActivity.start(SplashActivity.this, false);
                    finish();
                }
            } else if ("MainActivity".equals(activitySkip.getmActivity())) {
                MainActivity.start(SplashActivity.this, false);
                finish();
            }
        });
    }

    @Override
    protected void bindViewModel() {
        mDataBinding.setViewModel(mViewModel);
    }

    @Override
    protected void initViewModel() {
        mViewModel = new ViewModelProvider(this).get(SplashViewModel.class);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_splash;
    }
}