package com.gcy.wanandroid.ui.activity.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.gcy.wanandroid.R;
import com.gcy.wanandroid.base.BaseActivity;
import com.gcy.wanandroid.bean.responsebean.LoginBean;
import com.gcy.wanandroid.databinding.ActivityLoginBinding;
import com.gcy.wanandroid.ui.activity.main.MainActivity;
import com.gcy.wanandroid.ui.adapter.FmPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName LoginActivity
 * @Description 登录页面
 * @Author gcy
 * @Date 2023/6/18 0:50
 */

public class LoginActivity extends BaseActivity<ActivityLoginBinding, LoginViewModel> {

    public static void start(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void bindViewModel() {

    }

    @Override
    protected void initViewModel() {
        mViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.activity_login;
    }

    @Override
    protected void init() {
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(new FragmentLogin());
        fragments.add(new FragmentRegister());
        FmPagerAdapter adapter = new FmPagerAdapter(getSupportFragmentManager(), fragments);
        mDataBinding.viewPager.setAdapter(adapter);
        
        initData();
    }

    private void initData() {
        mViewModel.getUserBean().observe(this, new Observer<LoginBean>() {
            @Override
            public void onChanged(LoginBean loginBean) {
                if (loginBean != null) {
                    //登陆成功，跳转到首页
                    MainActivity.start(LoginActivity.this, true);
                    finish();
                }
            }
        });

        mViewModel.getRegisterStatus().observe(this, new Observer<Object>() {
            @Override
            public void onChanged(Object o) {
                //注册成功，返回到登录界面
                Toast.makeText(LoginActivity.this, "注册成功", Toast.LENGTH_SHORT).show();
                mViewModel.clearUserPwd();
                goToLogin();
            }
        });
    }

    /**
    * @Param
    * @return
     * 跳转到登录界面
    */

    public void goToLogin() {
        mDataBinding.viewPager.setCurrentItem(0);
    }

    public void goToRegister() {
        mViewModel.clearUserPwd();
        mDataBinding.viewPager.setCurrentItem(1);
    }
}