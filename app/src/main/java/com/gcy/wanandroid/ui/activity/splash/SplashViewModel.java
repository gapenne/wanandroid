package com.gcy.wanandroid.ui.activity.splash;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.gcy.wanandroid.R;
import com.gcy.wanandroid.base.viewmodel.BaseViewModel;
import com.gcy.wanandroid.bean.ActivitySkip;
import com.gcy.wanandroid.bean.responsebean.ImageBean;
import com.gcy.wanandroid.http.data.HttpDisposable;
import com.gcy.wanandroid.http.request.HttpFactory;
import com.gcy.wanandroid.http.request.HttpRequest;
import com.gcy.wanandroid.http.request.ServerAddress;
import com.gcy.wanandroid.util.NetworkUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Consumer;

/**
 * @author: gcy
 * @description:
 * @version: 1.0
 * @CreateTime: 2023-05-18  15:24
 */
public class SplashViewModel extends BaseViewModel {

    private MutableLiveData<ImageBean> mImage;
    private MutableLiveData<String> mTimer;
    private MutableLiveData<ActivitySkip> mActivitySkip;
    private String url;

    public SplashViewModel() {
        mActivitySkip = new MutableLiveData<>();
        mImage = new MutableLiveData<>();
        mTimer = new MutableLiveData<>();
        mTimer.postValue(getResources().getString(R.string.skip) + getResources().getString(R.string.time_5));
    }

    public LiveData<ImageBean> getImageData() {
        return mImage;
    }

    public LiveData<String> getTimer() {
        return mTimer;
    }

    public LiveData<ActivitySkip> getActivitySkip() {
        return mActivitySkip;
    }

    @Override
    public void onCreate(@NonNull LifecycleOwner owner) {
        super.onCreate(owner);
        startTimer();
        loadImageView();
    }

    /**
     * @author gcy
     * @description 获取每Bing每日照片
     * @date 2023/5/19 18:37
     */
    private void loadImageView() {
        if (!NetworkUtils.isConnected()) {
            //没有网络连接
            mImage.postValue(null);
        }else {
            HttpRequest.getInstance(ServerAddress.API_BING)
                    .getImage("js", 0, 1)
                    .compose(HttpFactory.schedulers())
                    .subscribe(new HttpDisposable<ImageBean>() {
                        @Override
                        public void success(ImageBean imageBean) {
                            mImage.postValue(imageBean);
                            url = imageBean.getImages().get(0).getCopyrightlink();
                        }

                        @Override
                        public void onError(Throwable e) {
                            mImage.postValue(null);
                        }
                    });
        }
    }

    /**
     * @author gcy
     * @description 开始倒计时
     * @date 2023/5/19 17:15
     */
    private void startTimer() {
        List<String> list = new ArrayList<>();
        for (int i = 4; i >= 0; i--) {
            list.add(i + getResources().getString(R.string.time_s));
        }
        Observable<String> observable = Observable.fromIterable(list);
        Observable<Long> time = Observable.interval(1, TimeUnit.SECONDS);
        Observable.zip(observable, time, new BiFunction<String, Long, String>() {

            @Override
            public String apply(String s, Long aLong) throws Exception {
                return s;
            }
        }).subscribe(new Consumer<String>() {
            @Override
            public void accept(String str) throws Exception {
                if ((getResources().getString(R.string.time_0)).equals(str)) {
                    startMainActivity();
                }
                mTimer.postValue(getResources().getString(R.string.skip) + str);
            }
        });
    }

    //跳转到主页
    public void startMainActivity() {
        ActivitySkip skip = new ActivitySkip();
        skip.setmActivity("MainActivity");
        mActivitySkip.postValue(skip);
    }

    //跳转到每日图片的详情界面
    public void startSplashImageDetail() {
        ActivitySkip skip = new ActivitySkip();
        skip.setmActivity("DetailsActivity");
        skip.setParam1(url);
        mActivitySkip.postValue(skip);
    }
}
