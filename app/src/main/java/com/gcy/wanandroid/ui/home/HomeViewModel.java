package com.gcy.wanandroid.ui.home;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.gcy.wanandroid.base.viewmodel.BaseViewModel;
import com.gcy.wanandroid.bean.responsebean.ArticleBean;
import com.gcy.wanandroid.bean.responsebean.ArticleListBean;
import com.gcy.wanandroid.bean.responsebean.HomeBanner;
import com.gcy.wanandroid.bean.responsebean.home.BannerData;
import com.gcy.wanandroid.bean.responsebean.home.HomeData;
import com.gcy.wanandroid.config.LoadState;
import com.gcy.wanandroid.http.data.HttpDisposable;
import com.gcy.wanandroid.http.request.HttpFactory;
import com.gcy.wanandroid.http.request.HttpRequest;
import com.gcy.wanandroid.util.NetworkUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: gcy
 * @description: 主页ViewModel
 * @version: 1.0
 * @CreateTime: 2023-06-27  11:38
 */
public class HomeViewModel extends BaseViewModel {

    private MutableLiveData<List<HomeData>> mHomeList;
    private List<HomeData> mList;

    private int mPageNum;

    public HomeViewModel() {
        mHomeList = new MutableLiveData<>();
        mList = new ArrayList<>();
    }

    public LiveData<List<HomeData>> getHomeList() {
        return mHomeList;
    }

    @Override
    public void reloadData() {
        mRefresh = false;
        loadHomeData();
    }

    /**
     * 获取首页数据
     */
    public void loadHomeData() {
        if (!mRefresh) {
            loadState.setValue(LoadState.LOADING);
        }
        mPageNum = 0;
        loadBanner();
    }

    /**
     * @author gcy
     * @description 获取首页轮播图
     * @date 2023/6/27 15:47
     */
    private void loadBanner() {
        if (NetworkUtils.isConnected() && NetworkUtils.getWifiEnabled()) {
            loadBannerByNet();
        } else {
            loadBannerByDb();
        }
    }

    /**
     * 从数据库获取Banner
     */
    private void loadBannerByDb() {

    }

    /**
     * @author gcy
     * @description 从网络接口获取Banner
     * @date 2023/6/27 15:52
     */
    private void loadBannerByNet() {
        HttpRequest.getInstance()
                .getBanner()
                .compose(HttpFactory.schedulers())
                .subscribe(new HttpDisposable<List<HomeBanner>>() {
                    @Override
                    public void success(List<HomeBanner> homeBanners) {
                        mList.clear();
                        HomeData homeData = new HomeData();
                        homeData.setBannerData(new BannerData(homeBanners));
                        mList.add(homeData);

                        if (!mRefresh) {
                            loadState.postValue(LoadState.SUCCESS);
                        }
                        //获取置顶文章
                        loadTopArticleList();
                    }

                    @Override
                    public void onError(Throwable e) {
                        loadTopArticleList();
                    }
                });
    }

    private void loadTopArticleList() {
        HttpRequest.getInstance()
                .getTopArticleList()
                .compose(HttpFactory.schedulers())
                .subscribe(new HttpDisposable<List<ArticleBean>>() {
                    @Override
                    public void success(List<ArticleBean> articleBeans) {
                        if (articleBeans != null && articleBeans.size() != 0) {
                            HomeData homeData = new HomeData();
                            HomeData.TopArticle topArticle = new HomeData.TopArticle();
                            topArticle.setName("置顶文章");
                            topArticle.setArticleBeanList(articleBeans);
                            homeData.setTopArticleList(topArticle);
                            mList.add(homeData);

                            //获取首页文章
                            loadArticleList(0);
                            if (!mRefresh) {
                                loadState.postValue(LoadState.SUCCESS);
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        loadArticleList(0);
                    }
                });
    }

    private void loadArticleList(int page) {
        HttpRequest.getInstance()
                .getArticleList(page)
                .compose(HttpFactory.schedulers())
                .subscribe(new HttpDisposable<ArticleListBean>() {
                    @Override
                    public void success(ArticleListBean articleListBean) {
                        if (articleListBean.getDatas() != null && articleListBean.getDatas().size() != 0) {
                            for (ArticleBean bean : articleListBean.getDatas()) {
                                HomeData homeData = new HomeData();
                                homeData.setArticleList(bean);
                                mList.add(homeData);
                            }

                            List<HomeData> homeData = new ArrayList<>();
                            homeData.addAll(mList);
                            mHomeList.postValue(homeData);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        loadState.postValue(LoadState.ERROR);
                    }
                });
    }

    /**
     * 刷新
     */
    public void refreshData(boolean refresh) {
        mRefresh = true;
        if (refresh) {
            mPageNum = 0;
            loadHomeData();
        } else {
            mPageNum++;
            loadArticleList(mPageNum);
        }
    }
}
