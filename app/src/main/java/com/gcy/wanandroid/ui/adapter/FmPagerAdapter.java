package com.gcy.wanandroid.ui.adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * 1. @ClassName FmPagerAdapter
 * 2. @Description TODO
 * 3. @Author gcy
 * 4. @Date 2023/6/18 1:08
 */
public class FmPagerAdapter extends FragmentPagerAdapter {

    private Fragment mCurrentFragment;
    private List<Fragment> fragments = new ArrayList<>();
    private List<String> sTitle= new ArrayList<>();

    public FmPagerAdapter(@NonNull FragmentManager mFragmentManager, List<Fragment> fragments) {
        super(mFragmentManager);
        this.fragments = fragments;
    }

    public FmPagerAdapter(@NonNull FragmentManager mFragmentManager, List<Fragment> fragments, List<String> list) {
        super(mFragmentManager);
        this.fragments = fragments;
        this.sTitle = list;
    }

    @NonNull
    @Override
    public Fragment getItem(int i) {
        return fragments.get(i);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (sTitle != null && sTitle.size() != 0) {
            return sTitle.get(position);
        } else {
            return null;
        }
    }

    @Override
    public void setPrimaryItem(@NonNull View container, int position, @NonNull Object object) {
        mCurrentFragment = (Fragment) object;
        super.setPrimaryItem(container, position, object);
    }

    public Fragment getCurrentFragment() {
        return mCurrentFragment;
    }
}
