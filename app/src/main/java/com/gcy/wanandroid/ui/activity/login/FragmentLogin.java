package com.gcy.wanandroid.ui.activity.login;

import android.content.Context;
import android.content.Intent;

import androidx.lifecycle.ViewModelProvider;

import com.gcy.wanandroid.R;
import com.gcy.wanandroid.base.BaseFragment;
import com.gcy.wanandroid.databinding.LoginFragmentBinding;

public class FragmentLogin extends BaseFragment<LoginFragmentBinding, LoginViewModel> {

    @Override
    protected void init() {

    }

    @Override
    protected void bindViewModel() {
        mDataBinding.setViewModel(mViewModel);
        mDataBinding.setActivity((LoginActivity) getActivity());
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.login_fragment;
    }

    @Override
    protected void initViewModel() {
        mViewModel = new ViewModelProvider(this).get(LoginViewModel.class);
    }
}