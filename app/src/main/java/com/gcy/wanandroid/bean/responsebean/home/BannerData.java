package com.gcy.wanandroid.bean.responsebean.home;

import com.gcy.wanandroid.bean.responsebean.HomeBanner;

import java.util.List;

/**
 * @author: gcy
 * @description: TODO
 * @version: 1.0
 * @CreateTime: 2023-06-27  13:15
 */
public class BannerData {

    private List<HomeBanner> bannerData;

    public BannerData() {
    }

    public BannerData(List<HomeBanner> bannerData) {
        this.bannerData = bannerData;
    }

    public List<HomeBanner> getBannerData() {
        return bannerData;
    }

    public void setBannerData(List<HomeBanner> bannerData) {
        this.bannerData = bannerData;
    }
}