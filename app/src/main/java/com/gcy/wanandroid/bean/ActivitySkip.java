package com.gcy.wanandroid.bean;

/**
 * @author: gcy
 * @description:
 * @version: 1.0
 * @CreateTime: 2023-05-19  17:00
 */
public class ActivitySkip {

    private String mActivity;
    private String param1;
    private boolean param2;
    private String param3;

    public String getmActivity() {
        return mActivity;
    }

    public void setmActivity(String mActivity) {
        this.mActivity = mActivity;
    }

    public String getParam1() {
        return param1;
    }

    public void setParam1(String param1) {
        this.param1 = param1;
    }

    public boolean isParam2() {
        return param2;
    }

    public void setParam2(boolean param2) {
        this.param2 = param2;
    }

    public String getParam3() {
        return param3;
    }

    public void setParam3(String param3) {
        this.param3 = param3;
    }
}
