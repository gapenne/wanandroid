package com.gcy.wanandroid.navinterface;

/**
 * @author: gcy
 * @description: 滚动到顶部接口
 * @version: 1.0
 * @CreateTime: 2023-05-31  18:26
 */
public interface ScrollToTop {
    //滚动到顶部
    void scrollToTop();
}
