package com.gcy.wanandroid.config;

/**
 * @author gcy
 * @description 加载状态
 * @date 2023/5/15 19:13
 */
public enum LoadState {

    /**
     * @description 加载中
     */
    LOADING,

    /**
     * @description 没有网络
     */
    NO_NETWORK,

    /**
     * @description 没有数据
     */
    NO_DATA,

    /**
     * @description 加载出错
     */
    ERROR,

    /**
     * @description 加载成功
     */
    SUCCESS
}
