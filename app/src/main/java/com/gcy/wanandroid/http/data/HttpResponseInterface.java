package com.gcy.wanandroid.http.data;

import com.google.gson.Gson;

/**
 * @author gcy
 * @description 请求响应接口
 * @date 2023/5/16 15:18
 */
public interface HttpResponseInterface {

    /**
     * @author gcy
     * @description 获取处理掉code和msg后的信息
     * @date 2023/5/16 15:23
     */
    String getResponseData(Gson gson, String response);
}
