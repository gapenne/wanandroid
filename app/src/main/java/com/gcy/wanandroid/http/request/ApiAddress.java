package com.gcy.wanandroid.http.request;

import com.gcy.wanandroid.bean.responsebean.ArticleBean;
import com.gcy.wanandroid.bean.responsebean.ArticleListBean;
import com.gcy.wanandroid.bean.responsebean.HomeBanner;
import com.gcy.wanandroid.bean.responsebean.ImageBean;
import com.gcy.wanandroid.bean.responsebean.LoginBean;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * @author: gcy
 * @description: 网络请求接口地址
 * @version: 1.0
 * @CreateTime: 2023-05-17  17:36
 */
public interface ApiAddress {

    /**
     * @author gcy
     * @description 获取闪屏页图片
     * @date 2023/5/19 18:40
     */
    @GET("HPImageArchive.aspx")
    Observable<ImageBean> getImage(@Query("format") String format, @Query("idx") int idx, @Query("n") int n);

    /**
     * @author gcy
     * @description 收藏文章
     * @date 2023/5/17 17:47
     */
    @POST("lg/collect/{id}/json")
    Observable<Response<Void>> collectArticle(@Path("id") int id);

    //取消收藏文章
    @POST("lg/uncollect_originId/{id}/json")
    Observable<Object> unCollectArticle(@Path("id") int id);

    //登录
    @FormUrlEncoded
    @POST("user/login")
    Observable<LoginBean> Login(@Field("username") String username, @Field("password") String password);

    //注册
    @FormUrlEncoded
    @POST("user/register")
    Observable<Object> register(@Field("username") String username,
                                @Field("password") String password,
                                @Field("repassword") String repwd);

    /**
     * 获取首页banner
     */
    @GET("banner/json")
    Observable<List<HomeBanner>> getBanner();

    /**
     * 获取置顶文章
     */
    @GET("article/top/json")
    Observable<List<ArticleBean>> getTopArticleList();

    /**
     * 获取首页文章列表
     */
    @GET("article/list/{page}/json")
    Observable<ArticleListBean> getArticleList(@Path("page") int page);
}
