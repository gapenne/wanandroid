package com.gcy.wanandroid.http.httptool;

import android.text.TextUtils;

/**
 * @author: gcy
 * @description: 自定义异常抛出
 * @version: 1.0
 * @CreateTime: 2023-05-16  16:38
 */
public class HttpException extends RuntimeException{

    private int code;
    private String message;

    public HttpException(String message) {
        this.message = message;
    }

    public HttpException(int code, String message) {
        this.message = message;
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return TextUtils.isEmpty(message) ? "" : message;
    }
}
