package com.gcy.wanandroid.http.httptool;

import android.util.Log;

import androidx.annotation.NonNull;

import com.gcy.wanandroid.bean.responsebean.LoginBean;
import com.gcy.wanandroid.config.Constants;
import com.gcy.wanandroid.util.NetworkUtils;
import com.orhanobut.hawk.Hawk;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashSet;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;

/**
 * @author: gcy
 * @description: 自定义请求拦截器
 * @version: 1.0
 * @CreateTime: 2023-05-16  15:32
 */
public class HttpInterceptor implements Interceptor {

    private static final Charset UTF8 = Charset.forName("UTF-8");

    private static String REQUEST_TAG = "请求";
    @NonNull
    @Override
    public Response intercept(@NonNull Chain chain) throws IOException {
        if (!NetworkUtils.isConnected()) {
            throw new HttpException("网络连接异常，请检查网络后重试");
        }

        Request request = chain.request();
        request = getHeaderRequest(request);
        logRequest(request);
        Response response = chain.proceed(request);
        if (!response.headers("Set-Cookies").isEmpty()) {
            HashSet<String> cookies = new HashSet<>();
            for (String header : response.headers("Set-Cookie")) {
                cookies.add(header);
            }
            Hawk.put(Constants.HawkCode.COOKIE, cookies);
        }
        logResponse(response);
        return response;
    }

    /**
     * @author gcy
     * @description 添加header
     * @date 2023/5/16 17:01
     */
    private Request getHeaderRequest(Request request) {
        LoginBean loginData = Hawk.get(Constants.HawkCode.LOGIN_DATA);
        Request headRequest;
        if (loginData != null) {
            headRequest = request
                    .newBuilder()
                    .addHeader("token", loginData.getToken())
                    .addHeader("Cookie", "loginUserName=" + loginData.getUsername())
                    .addHeader("Cookie", "loginUserPassword=" + loginData.getPassword())
                    .build();
        } else {
            headRequest = request
                    .newBuilder()
                    .addHeader("platform", "android")
                    .addHeader("version", "1.0")
                    .build();
        }
        return headRequest;
    }

    /**
     * @author gcy
     * @description 打印请求信息
     * @date 2023/5/17 10:28
     */
    private void logRequest(Request request) {
        Log.d(REQUEST_TAG + "method", request.method());
        Log.d(REQUEST_TAG + "url", request.url().toString());
        Log.d(REQUEST_TAG + "header", request.headers().toString());
        if (request.method().equals("GET")) {
            return;
        }
        try {
            RequestBody requestBody = request.body();
            String parameter = null;
            Buffer buffer = new Buffer();
            requestBody.writeTo(buffer);
            parameter = buffer.readString(UTF8);
            buffer.flush();
            buffer.close();
            Log.d(REQUEST_TAG + "参数", parameter);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * @author gcy
     * @description 打印返回结果
     * @date 2023/5/17 10:54
     */
    private void logResponse(Response response) {
        try {
            ResponseBody responseBody = response.body();
            String rBody = null;
            BufferedSource source = responseBody.source();
            source.request(Long.MAX_VALUE);
            Buffer buffer = source.getBuffer();
            Charset charset = UTF8;
            MediaType contentType = responseBody.contentType();
            if (contentType != null) {
                charset = contentType.charset(UTF8);
            }
            rBody = buffer.clone().readString(charset);
            Log.d(REQUEST_TAG + "返回值", rBody);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
