package com.gcy.wanandroid.http.data;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * @author: gcy
 * @description: 数据封装类型
 * @version: 1.0
 * @CreateTime: 2023-05-17  16:36
 */
public class HttpBaseResponse<T> implements Serializable {

    @Expose
    public int errorCode;
    @Expose
    public String errorMsg;
    @Expose
    public T data;
}
