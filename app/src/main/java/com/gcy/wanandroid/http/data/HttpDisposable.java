package com.gcy.wanandroid.http.data;

import io.reactivex.observers.DisposableObserver;

/**
 * @author: gcy
 * @description: 返回数据
 * @version: 1.0
 * @CreateTime: 2023-05-17  17:49
 */
public abstract class HttpDisposable<T> extends DisposableObserver<T> {

    public HttpDisposable() {

    }

    protected void onStart() {

    }

    public void onNext(T value) {
        success(value);
    }

    public void onError(Throwable e) {

    }

    public void onComplete() {

    }

    public abstract void success(T t);
}
