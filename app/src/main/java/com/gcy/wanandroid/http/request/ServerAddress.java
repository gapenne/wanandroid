package com.gcy.wanandroid.http.request;

/**
 * @author: gcy
 * @description: 服务器地址
 * @version: 1.0
 * @CreateTime: 2023-05-17  16:26
 */
public class ServerAddress {

    public static final String API_BING = "https://www.bing.com/";
    public static final String API_DEFAULT_HOST = "https://wanandroid.com/";

    public static final String getApiDefaultHost() {
        return API_DEFAULT_HOST;
    }
}
