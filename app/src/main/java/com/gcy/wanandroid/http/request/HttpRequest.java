package com.gcy.wanandroid.http.request;

/**
 * @author: gcy
 * @description: 网络请求
 * @version: 1.0
 * @CreateTime: 2023-05-17  17:34
 */
public class HttpRequest {

    private static ApiAddress Instance;

    public static ApiAddress getInstance() {
        if (Instance == null) {
            synchronized (HttpRequest.class) {
                if (Instance == null) {
                    Instance = HttpFactory.getInstance(ApiAddress.class);
                }
            }
        }
        return Instance;
    }

    public static ApiAddress getInstance(String url) {
        return HttpFactory.getChangeUrlInstance(url, ApiAddress.class);
    }
}
