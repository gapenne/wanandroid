package com.gcy.wanandroid.manager;

import android.app.Activity;

import java.lang.ref.WeakReference;

/**
 * @author: gcy
 * @description: Activity管理类
 * @version: 1.0
 * @CreateTime: 2023-05-15  20:03
 */
public class MyActivityManager {
    private static MyActivityManager sInstance = new MyActivityManager();

    //引用当前的Activity
    private WeakReference<Activity> sCurrentActivityWeakRef;

    private MyActivityManager(){

    }

    public static MyActivityManager getInstance() {
        return sInstance;
    }

    //获取当前的Activity
    public Activity getCurrentActivity() {
        Activity currentActivity = null;
        if (sCurrentActivityWeakRef != null){
            currentActivity = sCurrentActivityWeakRef.get();
        }
        return currentActivity;
    }

    //保存Activity
    public void setCurrentActivity(Activity activity) {
        sCurrentActivityWeakRef = new WeakReference<Activity>(activity);
    }

}
